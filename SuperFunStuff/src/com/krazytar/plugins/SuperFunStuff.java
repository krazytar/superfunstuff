package com.krazytar.plugins;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SuperFunStuff extends JavaPlugin implements Listener {
    ArrayList<Player> snowwalkers = new ArrayList<>();
    FileConfiguration config;
    
    @Override
    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
        config = getConfig();
        config.options().copyDefaults(true);
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String args[]) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            switch(label.toLowerCase()) {
                case "freeze":
                    freezePlayer(p);
                    break;
                case "unfreeze":
                    unfreezePlayer(p);
                    break;
                case "forcechat":
                    String str = "";
                    Player player = getServer().getPlayer(args[1]);
                    for(int i = 1; i < args.length; i++) {
                        str += args[i];
                    }
                    forceChat(player, str);
                    break;
                case "snowwalk":
                    snowwalk(p);
                    p.sendMessage("You are now snowwalking!");
                    break;
            }
            
        } else {
            
        }
        return true;
    }
    
    private void freezePlayer(Player p) {
        p.setWalkSpeed(0f);
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 250));
        p.sendMessage(config.getString("freeze-message"));
    }
    private void unfreezePlayer(Player p) {
        p.setWalkSpeed(0.2f);
        p.removePotionEffect(PotionEffectType.JUMP);
        p.sendMessage(config.getString("unfreeze-message"));
    }
    private void forceChat(Player p, String chat) {
        p.chat(chat);
    }
    private void snowwalk(Player p) {
        if(snowwalkers.contains(p)) {
            snowwalkers.remove(p);
        } else {
            snowwalkers.add(p);
        }
    }
    
    
    
    
    @EventHandler
    void onWeatherChange(WeatherChangeEvent e) {
        if(config.getBoolean("weather")) getServer().broadcastMessage(config.getString("weather-message"));
    }
    @EventHandler
    void onEnchantment(EnchantItemEvent e) {
        if(config.getBoolean("enchant")) getServer().broadcastMessage(config.getString("enchant-message"));
    }
    @EventHandler
    void onLightningStrike(LightningStrikeEvent e) {
        if(config.getBoolean("strike")) getServer().broadcastMessage(config.getString("strike-message"));
    }
    @EventHandler
    void onPlayerMove(PlayerMoveEvent e) {
        if(snowwalkers.contains(e.getPlayer())) {
            Player p = e.getPlayer();
            Location loc = p.getLocation();
            if(loc.getBlock().getType() == Material.AIR) {
                Location loc2 = e.getPlayer().getLocation();
                loc2.setY(loc2.getY() - 1);
                if(loc2.getBlock().getType() != Material.AIR) {
                    loc.getWorld().getBlockAt(loc).setType(Material.SNOW);
                }   
            }
        }
    }
}
